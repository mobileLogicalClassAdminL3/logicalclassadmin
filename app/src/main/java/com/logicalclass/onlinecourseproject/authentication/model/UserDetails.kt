package com.logicalclass.onlinecourseproject.authentication.model

import com.google.gson.annotations.SerializedName

data class UserDetails(
    val status: Int,
    val url: String?,
    val token: String,
    val tokenData: TokenData?,
    @SerializedName("msg")
    val errorMessage: String?
)

data class TokenData(
    val facultyAuth: String,
    val orgAuth: String
)

