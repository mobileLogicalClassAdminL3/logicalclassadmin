package com.logicalclass.onlinecourseproject.authentication.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.InputType
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.authentication.model.UserDetails
import com.logicalclass.onlinecourseproject.databinding.LoginLayoutBinding
import com.logicalclass.onlinecourseproject.home.MainActivity
import com.logicalclass.onlinecourseproject.models.SearchOrgResponse
import com.logicalclass.onlinecourseproject.repository.AppService.Factory.create
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.androidThread
import com.logicalclass.onlinecourseproject.repository.RxUtils.Companion.ioThread
import com.logicalclass.onlinecourseproject.utils.ForceUpdateChecker
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.hideKeyboard
import com.logicalclass.onlinecourseproject.utils.show
import com.logicalclass.onlinecourseproject.utils.showApiParsingErrorSnack
import com.logicalclass.onlinecourseproject.utils.showSnackMessage
import io.reactivex.disposables.CompositeDisposable

class LoginActivity : AppCompatActivity(), ForceUpdateChecker.OnUpdateNeededListener {
    private var orgObj: SearchOrgResponse? = null
    var tokenid: String? = ""
    private var disposable: CompositeDisposable? = null
    private lateinit var binding: LoginLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = LoginLayoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        disposable = CompositeDisposable()
        setClickListeners()
        orgObj = intent.extras?.getParcelable("organisation_obj")
        setOrganizationDetails()

        if (BuildConfig.DEBUG) {
            when (BuildConfig.FLAVOR) {
                "student" -> {
                    binding.edtUserName.setText("20AP705338")
                    binding.edtPassword.setText("20AP705338")
                }

                "faculty" -> {
                    binding.edtUserName.setText("demo@organisation.com")
                    binding.edtPassword.setText("1206")
                }

                else -> Unit
            }
        }
    }

    private fun setClickListeners() {
        binding.btnLogin.setOnClickListener {
            hideKeyboard()
            doLoginNew()
        }
        binding.txtForgotPassword.setOnClickListener { showForgotPasswordDialog() }
    }

    private fun doLoginNew() {
        disposable?.add(create().login(
            binding.edtUserName.text.toString().trim { it <= ' ' },
            binding.edtPassword.text.toString().trim { it <= ' ' },
            tokenid ?: "",
            SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.DEVICE_TOKEN)
        )

            .subscribeOn(ioThread()).doOnSubscribe {
                showProgress()
            }.observeOn(androidThread()).subscribe({ userDetails: UserDetails ->
                handleLoginResponse(userDetails)
            }, {
                hideProgress()
                binding.rootView.showApiParsingErrorSnack()
            })
        )
    }

    private fun hideProgress() {
        binding.rlProgress.hide()
    }

    private fun showProgress() {
        binding.rlProgress.show()
    }

    private fun handleLoginResponse(userDetails: UserDetails) {
        if (userDetails.status == 200) {
            SharedPrefsUtils.setStringPreference(
                this, SharedPrefsUtils.STUDENT_ID, binding.edtUserName?.text.toString().trim()
            )
            SharedPrefsUtils.setBooleanPreference(
                this@LoginActivity, SharedPrefsUtils.REMEMBER_ME, binding.ckbRememberMe.isChecked
            )
            SharedPrefsUtils.setStringPreference(
                this, SharedPrefsUtils.TOKEN_ID_2, userDetails.token
            )
            SharedPrefsUtils.setStringPreference(
                this, SharedPrefsUtils.FACULTY_AUTH, userDetails.tokenData?.facultyAuth
            )
            SharedPrefsUtils.setStringPreference(
                this, SharedPrefsUtils.ORG_AUTH, userDetails.tokenData?.orgAuth
            )

            val finalURL = "${BuildConfig.HTTP_KEY}${userDetails.url}"
            SharedPrefsUtils.setStringPreference(this, SharedPrefsUtils.WEB_VIEW_URL, finalURL)
            navigateToFacultyWebView(finalURL)
        } else {
            binding.rootView.showSnackMessage(
                userDetails.errorMessage ?: getString(R.string.something_went_wrong)
            )
        }
        hideProgress()
    }

    private fun navigateToFacultyWebView(finalURL: String) {
        val intent = Intent(this, MainActivity::class.java)
        intent.putExtra("webview_url", finalURL)
        intent.addFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        )
        startActivity(intent)
        finish()
    }

    private fun showForgotPasswordDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Forgot Password")
        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT
        val lp = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
        )
        lp.setMargins(20, 20, 20, 20)
        input.layoutParams = lp
        val linearLayout = LinearLayout(this)
        linearLayout.addView(input)
        builder.setView(linearLayout)
        builder.setPositiveButton(
            getString(android.R.string.ok)
        ) { _, _ -> requestForForgotPassword(input.text.toString()) }
        builder.setNegativeButton(
            android.R.string.no
        ) { dialog, _ -> dialog.cancel() }
        builder.show()
    }

    private fun requestForForgotPassword(uniqueId: String) {
        disposable?.add(create().forgotPassword(uniqueId).subscribeOn(ioThread()).doOnSubscribe {
            showProgress()
        }.observeOn(androidThread()).subscribe({ userDetails: UserDetails ->
            handleForgotPasswordResponse(userDetails)
        }, {
            hideProgress()
            binding.rootView.showApiParsingErrorSnack()
        })
        )
    }

    private fun handleForgotPasswordResponse(userDetails: UserDetails) {
        if (userDetails.status == 200) {
            binding.rootView.showSnackMessage(
                userDetails.errorMessage ?: getString(R.string.something_went_wrong)
            )
        } else {
            binding.rootView.showSnackMessage(
                userDetails.errorMessage ?: getString(R.string.something_went_wrong)
            )
        }
        hideProgress()
    }

    private fun setOrganizationDetails() {
        if (BuildConfig.FLAVOR == "faculty") {
            binding.txtLoginForLabel.text = "Login for Faculty / Organisation"
        } else {
            binding.txtLoginForLabel.text = "Login for Student"
        }

        orgObj?.let {
            tokenid = it.token
            val imgURL = BuildConfig.HTTP_KEY + it.img
            Glide.with(this).load(imgURL).into(binding.imgOrgLogo)
            binding.tvOrgName.text = getString(R.string.welcome_to_org, it.orgName)
            binding.tvOrgName.show()
        }
    }

    private fun checkPlayStoreUpdate() {
        if (!BuildConfig.DEBUG) ForceUpdateChecker.with(this).onUpdateNeeded(this).check()
    }

    override fun onUpdateNeeded(updateUrl: String?, mandatoryUpdate: Boolean?) {
        val builder = AlertDialog.Builder(this)
        if (mandatoryUpdate == true) {
            builder.setCancelable(false)
        } else {
            builder.setCancelable(true)
            builder.setNegativeButton("No, Thanks") { dialog, _ ->
                dialog.dismiss()
            }
        }
        builder.setTitle("New version available")
        builder.setPositiveButton("Update") { dialog, _ ->
            dialog.dismiss()
            redirectStore(updateUrl)
        }
        builder.setMessage(getString(R.string.update_available_description_dialog)).show()
    }

    private fun redirectStore(updateUrl: String?) {
        val intent = Intent(Intent.ACTION_VIEW).setData(Uri.parse(updateUrl))
        startActivity(intent)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (disposable != null) {
            disposable?.clear()
        }
    }
}