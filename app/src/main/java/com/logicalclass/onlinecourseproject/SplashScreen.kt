package com.logicalclass.onlinecourseproject

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.logicalclass.onlinecourseproject.databinding.SplashscreenBinding
import com.logicalclass.onlinecourseproject.home.MainActivity
import com.logicalclass.onlinecourseproject.service.MyFirebaseMessagingService.Companion.NOTIFICATION_TYPE
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils

class SplashScreen : AppCompatActivity() {
    private lateinit var binding: SplashscreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SplashscreenBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        //showLogoAnimation()
        runSplashTimer()
    }

    private fun showLogoAnimation() {/*Glide.with(this)
            .asGif()
            .load(R.raw.splash_animation)
            .listener(object : RequestListener<GifDrawable?> {
                override fun onLoadFailed(
                    e: GlideException?, model: Any?,
                    target: Target<GifDrawable?>?,
                    isFirstResource: Boolean
                ): Boolean {
                    goWithHandlerIfAnimationFails()
                    return false
                }

                override fun onResourceReady(
                    resource: GifDrawable?, model: Any?,
                    target: Target<GifDrawable?>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    resource?.setLoopCount(1)
                    resource?.registerAnimationCallback(object :
                        Animatable2Compat.AnimationCallback() {
                        override fun onAnimationEnd(drawable: Drawable?) {
                            handleNotificationClickFlow(intent.extras)
                        }
                    })
                    return false
                }
            }).into(binding.imgLogo)*/
    }

    // Using handler with postDelayed called runnable run method
    private fun runSplashTimer() {
        //binding.imgLogo.setImageResource(R.drawable.logo2)
        Handler(Looper.getMainLooper()).postDelayed(
            {
                handleNotificationClickFlow(intent.extras)
            }, 2 * 1000.toLong()
        )
    }

    private fun handleNotificationClickFlow(bundle: Bundle?) {
        bundle?.let {
            if (it.containsKey(NOTIFICATION_TYPE)) {
                val intent = when (it.getString(NOTIFICATION_TYPE)) {
                    else -> Intent(this, SplashScreen::class.java)
                }
                startNewActivity(intent)
            } else {
                handleRegularFlow()
            }
        } ?: kotlin.run {
            handleRegularFlow()
        }
    }

    private fun handleRegularFlow() {
        val intent: Intent = when {
            SharedPrefsUtils.getBooleanPreference(
                this, SharedPrefsUtils.REMEMBER_ME, false
            ) -> {
                val webViewUrl =
                    SharedPrefsUtils.getStringPreference(this, SharedPrefsUtils.WEB_VIEW_URL)
                val intent = Intent(this@SplashScreen, MainActivity::class.java)
                intent.putExtra("webview_url", webViewUrl)
                intent.putExtra("coming_from", "SplashScreen")
                intent
            }

            else -> {
                Intent(this, SearchOrganisationActivity::class.java)
            }
        }
        startNewActivity(intent)
    }

    private fun startNewActivity(intent: Intent) {
        intent.addFlags(
            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        )
        startActivity(intent)
        finish()
    }
}