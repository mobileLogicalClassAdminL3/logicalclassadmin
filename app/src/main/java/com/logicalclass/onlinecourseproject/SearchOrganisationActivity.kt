package com.logicalclass.onlinecourseproject

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.logicalclass.onlinecourseproject.databinding.ActivitySearchOrgBinding
import com.logicalclass.onlinecourseproject.authentication.view.LoginActivity
import com.logicalclass.onlinecourseproject.models.SearchOrgResponse
import com.logicalclass.onlinecourseproject.repository.AppService
import com.logicalclass.onlinecourseproject.repository.RxUtils
import com.logicalclass.onlinecourseproject.utils.*
import io.reactivex.disposables.CompositeDisposable


class SearchOrganisationActivity : AppCompatActivity(), ForceUpdateChecker.OnUpdateNeededListener {
    private var disposable: CompositeDisposable? = null
    private lateinit var binding: ActivitySearchOrgBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchOrgBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setToolbarData()
        disposable = CompositeDisposable()
        binding.btnSubmit.setOnClickListener {
            hideKeyboard()
            val orgName = binding.edtSearch.text.toString().trim()
            if (orgName.isNotEmpty()) {
                requestForSearchOrganization(orgName)
            } else {
                binding.rootView.showSnackMessage(getString(R.string.please_enter_organization_name))
            }
        }

        if (BuildConfig.DEBUG) {
            binding.edtSearch.setText("demoorganisation")
        }
        checkPlayStoreUpdate()
    }

    private fun checkPlayStoreUpdate() {
        if (!BuildConfig.DEBUG)
            ForceUpdateChecker.with(this).onUpdateNeeded(this).check()
    }

    private fun setToolbarData() {
        supportActionBar?.title = getString(R.string.title_search_organization)
    }

    private fun requestForSearchOrganization(orgName: String) {
        disposable?.clear()
        disposable?.add(
            AppService.create().getCheckOrganisation(orgName)
                .subscribeOn(RxUtils.ioThread())
                .doOnSubscribe {
                    showProgress()
                }.observeOn(RxUtils.androidThread())
                .subscribe({ response: SearchOrgResponse ->
                    handleSearchOrgResponse(response)
                }, {
                    hideProgress()
                    binding.rootView.showApiParsingErrorSnack()
                })
        )
    }

    private fun handleSearchOrgResponse(response: SearchOrgResponse) {
        if (response.status == 200) {
            SharedPrefsUtils.setStringPreference(this, SharedPrefsUtils.ORG_NAME, response.orgName)

            val intent = Intent(this, LoginActivity::class.java)
            val bundle = Bundle()
            bundle.putParcelable("organisation_obj", response)
            intent.putExtras(bundle)
            startActivity(intent)
            finish()
        } else {
            binding.rootView.showSnackMessage(
                response.message ?: getString(R.string.something_went_wrong)
            )
        }
        hideProgress()
    }

    private fun hideProgress() {
        binding.rlProgress.hide()
    }

    private fun showProgress() {
        binding.rlProgress.show()
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable?.clear()
    }

    override fun onUpdateNeeded(updateUrl: String?, mandatoryUpdate: Boolean?) {
        val builder = AlertDialog.Builder(this)
        if (mandatoryUpdate == true) {
            builder.setCancelable(false)
        } else {
            builder.setCancelable(true)
            builder.setNegativeButton("No, Thanks") { dialog, _ ->
                dialog.dismiss()
            }
        }
        builder.setTitle("New version available")
        builder.setPositiveButton("Update") { dialog, _ ->
            dialog.dismiss()
            redirectStore(updateUrl)
        }
        builder.setMessage(getString(R.string.update_available_description_dialog))
            .show()
    }

    private fun redirectStore(updateUrl: String?) {
        val intent = Intent(Intent.ACTION_VIEW).setData(Uri.parse(updateUrl))
        startActivity(intent)
    }
}