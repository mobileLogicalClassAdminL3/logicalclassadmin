package com.logicalclass.onlinecourseproject.home

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.webkit.*
import android.widget.FrameLayout
import androidx.activity.result.ActivityResult
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider.getUriForFile
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.logicalclass.onlinecourseproject.BuildConfig
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.databinding.ActivityMainBinding
import com.logicalclass.onlinecourseproject.databinding.LayoutAttachmentBottomSheetBinding
import com.logicalclass.onlinecourseproject.utils.hide
import com.logicalclass.onlinecourseproject.utils.show
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


class MainActivity : AppCompatActivity() {

    var myRequest: PermissionRequest? = null
    private lateinit var binding: ActivityMainBinding
    private lateinit var requestPermissionLauncher: ActivityResultLauncher<Array<String>>
    private var fileUploadCallback: ValueCallback<Array<Uri>>? = null
    private lateinit var currentPhotoUri: Uri
    private lateinit var bottomSheetDialog: BottomSheetDialog

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        showProgress()

        binding.webView.settings.javaScriptEnabled = true
        binding.webView.settings.loadWithOverviewMode = true
        binding.webView.settings.useWideViewPort = true
        binding.webView.settings.userAgentString =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36"
        binding.webView.settings.javaScriptCanOpenWindowsAutomatically = true
        binding.webView.settings.mediaPlaybackRequiresUserGesture = false
        binding.webView.settings.allowFileAccess = true;
        binding.webView.settings.allowContentAccess = true;

        binding.webView.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                hideProgress()
            }

            override fun shouldOverrideUrlLoading(
                view: WebView?, request: WebResourceRequest?
            ): Boolean {
                if (request?.url.toString().startsWith("tel:") || request?.url.toString()
                        .startsWith("whatsapp:")
                ) {
                    val intent = Intent(Intent.ACTION_VIEW)
                    intent.data = Uri.parse(request?.url.toString())
                    startActivity(intent)
                    return true
                }
                return false
            }

            override fun onReceivedError(
                view: WebView?, request: WebResourceRequest?, error: WebResourceError?
            ) {
                super.onReceivedError(view, request, error)
                hideProgress()
            }
        }

        binding.webView.webChromeClient = object : WebChromeClient() {
            private var mCustomView: View? = null
            private var mCustomViewCallback: CustomViewCallback? = null
            protected var mFullscreenContainer: FrameLayout? = null
            private var mOriginalOrientation = 0
            private var mOriginalSystemUiVisibility = 0
            override fun getDefaultVideoPoster(): Bitmap? {
                return if (mCustomView == null) {
                    null
                } else BitmapFactory.decodeResource(resources, 2130837573)
            }

            override fun onHideCustomView() {
                (window.decorView as FrameLayout).removeView(mCustomView)
                mCustomView = null
                window.decorView.systemUiVisibility = mOriginalSystemUiVisibility
                requestedOrientation = mOriginalOrientation
                mCustomViewCallback?.onCustomViewHidden()
                mCustomViewCallback = null
            }

            override fun onShowCustomView(
                paramView: View?, paramCustomViewCallback: CustomViewCallback?
            ) {
                if (mCustomView != null) {
                    onHideCustomView()
                    return
                }
                mCustomView = paramView
                mOriginalSystemUiVisibility = window.decorView.systemUiVisibility
                mOriginalOrientation = requestedOrientation
                mCustomViewCallback = paramCustomViewCallback
                (window.decorView as FrameLayout).addView(
                    mCustomView, FrameLayout.LayoutParams(-1, -1)
                )
                window.decorView.systemUiVisibility = 3846 or View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            }

            override fun onPermissionRequest(request: PermissionRequest) {
                myRequest = request
                for (permission in request.resources) {
                    when (permission) {
                        PermissionRequest.RESOURCE_VIDEO_CAPTURE, PermissionRequest.RESOURCE_AUDIO_CAPTURE -> {
                            request.grant(
                                arrayOf(
                                    PermissionRequest.RESOURCE_AUDIO_CAPTURE,
                                    PermissionRequest.RESOURCE_VIDEO_CAPTURE
                                )
                            )
                        }
                    }
                }
            }

            override fun onShowFileChooser(
                webView: WebView?,
                filePathCallback: ValueCallback<Array<Uri>>?,
                fileChooserParams: FileChooserParams?
            ): Boolean {
                fileUploadCallback?.onReceiveValue(null)
                fileUploadCallback = filePathCallback

                showAttachmentsPickerBottomSheet()
                return true
            }

            private fun showAttachmentsPickerBottomSheet() {
                val bottomSheetViewBinding =
                    LayoutAttachmentBottomSheetBinding.inflate(layoutInflater)
                bottomSheetDialog = BottomSheetDialog(this@MainActivity)
                bottomSheetDialog.setContentView(bottomSheetViewBinding.root)
                bottomSheetDialog.show()
                bottomSheetDialog.setOnDismissListener {
                    fileUploadCallback?.onReceiveValue(null)
                    fileUploadCallback = null
                }

                with(bottomSheetViewBinding) {
                    imgDocument.setOnClickListener {
                        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                            intent.addCategory(Intent.CATEGORY_OPENABLE)
                            type = "application/pdf"
                        }
                        medialChooser.launch(intent)
                    }
                    imgCamera.setOnClickListener {
                        openCamera()
                    }
                    imgImage.setOnClickListener {
                        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                            intent.addCategory(Intent.CATEGORY_OPENABLE)
                            type = "image/*"
                        }
                        medialChooser.launch(intent)
                    }
                    imgVideo.setOnClickListener {
                        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                            intent.addCategory(Intent.CATEGORY_OPENABLE)
                            type = "video/*"
                        }
                        medialChooser.launch(intent)
                    }
                    imgAudio.setOnClickListener {
                        val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                            intent.addCategory(Intent.CATEGORY_OPENABLE)
                            type = "audio/*"
                        }
                        medialChooser.launch(intent)
                    }
                }
            }
        }

        askForRequiredPermissions()
    }

    private fun loadWebUrl() {
        val url = intent.getStringExtra("webview_url") ?: "https://www.logicalclass.com/"
        Log.e("MainActivity:webview url-", url)
        binding.webView.loadUrl(url)
    }

    private fun hideProgress() {
        binding.rlProgress.hide()
    }

    private fun showProgress() {
        binding.rlProgress.show()
    }

    private fun askForRequiredPermissions() {
        val notificationPermissionResult =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                ContextCompat.checkSelfPermission(
                    this, Manifest.permission.POST_NOTIFICATIONS
                ) == PackageManager.PERMISSION_GRANTED
            } else {
                true
            }
        val readPermissionResult = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_MEDIA_IMAGES
            ) + ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_MEDIA_VIDEO
            ) == PackageManager.PERMISSION_GRANTED
        } else {
            ContextCompat.checkSelfPermission(
                this, Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED
        }

        val recordPermissionResult = ContextCompat.checkSelfPermission(
            this, Manifest.permission.RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
        val cameraPermissionResult = ContextCompat.checkSelfPermission(
            this, Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED

        requestPermissionLauncher =
            registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
                var isAllPermissionsGranted = true
                permissions.entries.forEach {
                    Log.e("DEBUG", "${it.key} = ${it.value}")
                    isAllPermissionsGranted = isAllPermissionsGranted && it.value
                }

                if (isAllPermissionsGranted) {
                    myRequest?.grant(myRequest?.resources)
                    loadWebUrl()
                } else {
                    showPermissionConfirmationDialog(
                        "We must require these permissions. Please allow from settings."
                    ) { _, _ ->
                        val intent = Intent(
                            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                            Uri.parse("package:$packageName")
                        )
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    }
                }
            }


        when {
            notificationPermissionResult && readPermissionResult && recordPermissionResult && cameraPermissionResult -> {
                myRequest?.grant(myRequest?.resources)
                loadWebUrl()
            }

            shouldShowRequestPermissionRationale(Manifest.permission.POST_NOTIFICATIONS) -> {
                showPermissionConfirmationDialog(
                    "We must require these permissions. Please allow from settings."
                ) { _, _ ->
                    val intent = Intent(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:$packageName")
                    )
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                }
            }

            else -> {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                    requestPermissionLauncher.launch(
                        arrayOf(
                            Manifest.permission.READ_MEDIA_IMAGES,
                            Manifest.permission.READ_MEDIA_VIDEO,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.CAMERA,
                            Manifest.permission.POST_NOTIFICATIONS
                        )
                    )
                } else {
                    requestPermissionLauncher.launch(
                        arrayOf(
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.CAMERA
                        )
                    )
                }
            }
        }
    }

    private val medialChooser =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            fileUploadCallback = if (result.resultCode == RESULT_OK) {
                val galleryResultData = if (result.data == null || result.data?.data == null) null
                else arrayOf(result.data?.data!!)
                fileUploadCallback?.onReceiveValue(galleryResultData)
                fileUploadCallback = null
                null
            } else {
                fileUploadCallback?.onReceiveValue(null)
                null
            }
            if (this::bottomSheetDialog.isInitialized) {
                bottomSheetDialog.dismiss()
            }
        }

    private val takePicture: ActivityResultLauncher<Uri> =
        registerForActivityResult(ActivityResultContracts.TakePicture()) { success ->
            if (success) {
                currentPhotoUri.let { uri ->
                    val result = arrayOf(uri)
                    fileUploadCallback?.onReceiveValue(result)
                    fileUploadCallback = null
                }
            } else {
                fileUploadCallback?.onReceiveValue(null)
                fileUploadCallback = null
            }
            if (this::bottomSheetDialog.isInitialized) {
                bottomSheetDialog.dismiss()
            }
        }

    private fun openCamera() {
        currentPhotoUri = getUriForFile(this, BuildConfig.APPLICATION_ID, createImageFile())
        grantUriPermission(packageName, currentPhotoUri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
        takePicture.launch(currentPhotoUri)
    }

    private fun createImageFile(): File {
        // Create a unique image file name
        val timeStamp: String =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        )
    }

    private fun showPermissionConfirmationDialog(
        message: String, onClickListener: (Any, Any) -> Unit
    ) {
        if (!isFinishing) AlertDialog.Builder(this).setMessage(message)
            .setPositiveButton("Go to Settings", onClickListener)
            .setNegativeButton(getString(R.string.no)) { _, _ -> finish() }.show()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (binding.webView.canGoBack()) {
                        binding.webView.goBack()
                    } else {
                        finish()
                    }
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }
}