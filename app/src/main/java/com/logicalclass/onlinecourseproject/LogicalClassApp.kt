package com.logicalclass.onlinecourseproject

import android.app.Activity
import android.app.Application
import android.os.Bundle
import android.view.WindowManager
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings

class LogicalClassApp : Application() {

    override fun onCreate() {
        super.onCreate()
        initFirebaseRemoteConfig()
        setupActivityListener()
    }

    private fun initFirebaseRemoteConfig() {
        val mFirebaseRemoteConfig: FirebaseRemoteConfig? = FirebaseRemoteConfig.getInstance()
        val configSettings =
            FirebaseRemoteConfigSettings.Builder().setMinimumFetchIntervalInSeconds(100).build()
        mFirebaseRemoteConfig?.setConfigSettingsAsync(configSettings)
        mFirebaseRemoteConfig?.setDefaultsAsync(R.xml.remote_config_defaults)
        mFirebaseRemoteConfig?.fetchAndActivate()?.addOnCompleteListener {
            if (it.isSuccessful) {
                val result = it.result
            } else {

            }
        }
    }

    private fun setupActivityListener() {
        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                activity.window.setFlags(
                    WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE
                )
            }

            override fun onActivityStarted(p0: Activity) {}
            override fun onActivityResumed(p0: Activity) {}
            override fun onActivityPaused(p0: Activity) {}
            override fun onActivityStopped(p0: Activity) {}
            override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {}
            override fun onActivityDestroyed(p0: Activity) {}
        })
    }
}