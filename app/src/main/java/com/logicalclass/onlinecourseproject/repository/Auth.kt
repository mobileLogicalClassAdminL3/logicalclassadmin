package com.logicalclass.onlinecourseproject.repository

import com.google.gson.annotations.SerializedName

data class Auth(
        val StudentAuth: String,
        @SerializedName("class")
        val className: String,
        val orgAuth: String
)

data class Id(
        val `$id`: String
)