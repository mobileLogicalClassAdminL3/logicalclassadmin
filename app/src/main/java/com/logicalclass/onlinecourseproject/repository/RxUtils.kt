package com.logicalclass.onlinecourseproject.repository

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * This class is used to hold the utility methods for RXAndroid.
 */
class RxUtils {
    companion object {
        /**
         * Shortcut of Schedulers.io()
         * @return the result of Schedulers.io()
         * @see Schedulers.io()
         */
        fun ioThread(): Scheduler {
            return Schedulers.io()
        }

        /**
         * Shortcut of AndroidSchedulers.mainThread()
         * @return the result of AndroidSchedulers.mainThread()
         * @see AndroidSchedulers.mainThread()
         */
        fun androidThread(): Scheduler {
            return AndroidSchedulers.mainThread()
        }

        /**
         * This method is used to dispose the CompositeDisposable.
         * @param subscription - Reference for the CompositeDisposable.
         */
        fun disposeIfNotNull(subscription: CompositeDisposable?) {
            subscription?.dispose()
        }
    }
}