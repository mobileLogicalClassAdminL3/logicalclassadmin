package com.logicalclass.onlinecourseproject.models

import android.os.Parcel
import android.os.Parcelable

data class SearchOrgResponse(
    val img: String,
    val message: String?,
    val orgAuth: String,
    val orgName: String,
    val orgShort: String,
    val status: Int,
    val token: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readString() ?: ""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(img)
        parcel.writeString(message)
        parcel.writeString(orgAuth)
        parcel.writeString(orgName)
        parcel.writeString(orgShort)
        parcel.writeInt(status)
        parcel.writeString(token)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SearchOrgResponse> {
        override fun createFromParcel(parcel: Parcel): SearchOrgResponse {
            return SearchOrgResponse(parcel)
        }

        override fun newArray(size: Int): Array<SearchOrgResponse?> {
            return arrayOfNulls(size)
        }
    }
}