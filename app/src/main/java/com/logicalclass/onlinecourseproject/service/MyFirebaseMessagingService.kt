package com.logicalclass.onlinecourseproject.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.logicalclass.onlinecourseproject.R
import com.logicalclass.onlinecourseproject.SplashScreen
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils
import com.logicalclass.onlinecourseproject.utils.SharedPrefsUtils.DEVICE_TOKEN

class MyFirebaseMessagingService : FirebaseMessagingService() {

    companion object {
        private val TAG = "MyFirebaseMsgService"
        const val EXAM_ID = "examId"
        const val CLASS_ID = "classId"
        const val NOTIFICATION_TYPE = "type"

        const val TYPE_EXAM = "EXAM"
        const val TYPE_CLASS = "CLASS"
        const val TYPE_PUBLIC_ANNOUNCEMENTS = "ANNOUNCEMENT"
        const val TYPE_ATTENDANCE = "ATTENDANCE"
        const val TYPE_DIARY = "DIARY"
        const val TYPE_ONLINE_COURSE = "ONLINE_COURSE"

        /*Online Tests Notification types*/
        const val TYPE_ONLINE_TEST_SCHEDULED = "ONLINE_TEST_SCHEDULED"
        const val TYPE_ONLINE_TEST_RESCHEDULED = "ONLINE_TEST_RESCHEDULED"
        const val TYPE_ONLINE_TEST_NOT_ATTEMPTED = "ONLINE_TEST_NOT_ATTEMPTED"
        const val TYPE_PRE_ONLINE_TEST_ALERT = "PRE_ONLINE_TEST_ALERT"
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.e("onMessageReceived", "called")
        Log.e("remoteMessage_data", remoteMessage.data.toString())
        Log.e("remoteMessage_title", remoteMessage.data["title"] ?: "null")
        Log.e("remoteMessage_body", remoteMessage.data["body"] ?: "null")
        sendNotification(remoteMessage)
    }

    override fun onNewToken(token: String) {
        Log.d("Refreshed Token", token)
        SharedPrefsUtils.setStringPreference(applicationContext, DEVICE_TOKEN, token)
    }

    private fun sendNotification(remoteMessage: RemoteMessage) {
        val intent: Intent = if (SharedPrefsUtils.isUserLoggedIn(applicationContext)
            && remoteMessage.data.isNotEmpty()
            && remoteMessage.data.containsKey(NOTIFICATION_TYPE)
        ) {
            handleNotificationFlow(remoteMessage)
        } else {
            Intent(this, SplashScreen::class.java)
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */,
            intent, PendingIntent.FLAG_ONE_SHOT)

        val channelId = getString(R.string.default_notification_channel_id)
        val channelName = getString(R.string.default_notification_channel_name)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_notification)
            .setColor(ContextCompat.getColor(this, R.color.tool_bar_start_color))
            .setContentTitle(remoteMessage.data["title"] ?: remoteMessage.notification?.title
            ?: "")
            .setContentText(remoteMessage.data["body"] ?: remoteMessage.notification?.body
            ?: "")
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)
            .setPriority(NotificationCompat.PRIORITY_HIGH)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId, channelName, NotificationManager.IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    private fun handleNotificationFlow(remoteMessage: RemoteMessage): Intent {
        return when (remoteMessage.data[NOTIFICATION_TYPE]) {
            else ->
                Intent(this, SplashScreen::class.java)
        }
    }
}
