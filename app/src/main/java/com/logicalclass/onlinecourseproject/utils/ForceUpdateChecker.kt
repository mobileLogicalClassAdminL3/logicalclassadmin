package com.logicalclass.onlinecourseproject.utils

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import com.google.firebase.remoteconfig.FirebaseRemoteConfig

class ForceUpdateChecker(
        private val context: Context,
        private val onUpdateNeededListener: OnUpdateNeededListener?
) {

    interface OnUpdateNeededListener {
        fun onUpdateNeeded(updateUrl: String?, mandatoryUpdate: Boolean?)
        /*fun updateNotAvailable()*/
    }

    fun check() {
        val remoteConfig = FirebaseRemoteConfig.getInstance()
        if (remoteConfig.getBoolean(KEY_UPDATE_REQUIRED)) {
            val mandatoryUpdate = remoteConfig.getBoolean(MANDATORY_UPDATE)
            val currentVersion = remoteConfig.getString(KEY_CURRENT_VERSION).toLong()
            val appVersion = getAppVersion(context)
            val updateUrl = remoteConfig.getString(KEY_UPDATE_URL)
            if (currentVersion > appVersion && onUpdateNeededListener != null
            ) {
                onUpdateNeededListener.onUpdateNeeded(updateUrl, mandatoryUpdate)
            }
            /*else {
                onUpdateNeededListener?.updateNotAvailable()
            }*/
        }
    }

    private fun getAppVersion(context: Context): Long {
        var result = -1L
        try {
            result = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                context.packageManager.getPackageInfo(context.packageName, 0).longVersionCode
            } else {
                context.packageManager.getPackageInfo(context.packageName, 0).versionCode.toLong()
            }
        } catch (e: PackageManager.NameNotFoundException) {
        }
        return result
    }

    class Builder(private val context: Context) {
        private var onUpdateNeededListener: OnUpdateNeededListener? = null
        fun onUpdateNeeded(onUpdateNeededListener: OnUpdateNeededListener?): Builder {
            this.onUpdateNeededListener = onUpdateNeededListener
            return this
        }

        fun build(): ForceUpdateChecker {
            return ForceUpdateChecker(context, onUpdateNeededListener)
        }

        fun check(): ForceUpdateChecker {
            val forceUpdateChecker = build()
            forceUpdateChecker.check()
            return forceUpdateChecker
        }
    }

    companion object {
        private val TAG = ForceUpdateChecker::class.java.simpleName
        const val KEY_UPDATE_REQUIRED = "force_update_required"
        const val MANDATORY_UPDATE = "mandatory_update"
        const val KEY_CURRENT_VERSION = "force_update_current_version"
        const val KEY_UPDATE_URL = "force_update_store_url"
        fun with(context: Context): Builder {
            return Builder(context)
        }
    }

}