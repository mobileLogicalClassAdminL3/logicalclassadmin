package com.logicalclass.onlinecourseproject.utils

import android.content.Context
import android.os.Environment
import android.util.Log
import androidx.core.content.ContextCompat
import java.io.File

object FileUtils {

    fun getOnlineClassDownloadFileName(batchName: String, subject: String, dateTime: String): String {
//        return it.batch.plus(it.subject).plus(it.dateTime).plus(".pdf")
        return batchName.plus("##").plus(subject).plus("##").plus(dateTime)
    }

    /*return name of pdf file to be load from assets*/
    fun getPdfNameFromAssets(): String {
        return ""
    }

    /*return constant web pdf url*/
    fun getPdfUrl(): String {
        return ""
    }

    fun getOnlineClassesDocumentsPath(context: Context): String {
        return if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {
            val path = ContextCompat.getExternalFilesDirs(
                    context.applicationContext,
                    null
            )[0].absolutePath
            val userWiseFolder =
                SharedPrefsUtils.getStringPreference(context, SharedPrefsUtils.FACULTY_AUTH)
            val file = File(path, "OnlineClassesDocuments/$userWiseFolder")
            file.mkdir()
            /*val folderUnderAnother = File(file.path, userWiseFolder ?: "FallbackFolder")
            folderUnderAnother.absolutePath*/
            file.absolutePath
        } else {
            context.applicationContext.filesDir.absolutePath
        }
    }

    fun getRootDirPath(context: Context): String {
        return context.applicationContext.filesDir.absolutePath
    }

    fun clearOnlineClassesDocuments(context: Context) {
        val folderToDelete = File(getOnlineClassesDocumentsPath(context))
        if (folderToDelete.isDirectory) {
            val children: Array<String>? = folderToDelete.list()
            children?.let {
                for (i in it.indices) {
                    if (File(folderToDelete, it[i]).delete()) {
                        Log.e("File Delete Status", "Deleted")
                    } else {
                        Log.e("File Delete Status", "Not Deleted")
                    }
                }
            }
        }
    }
}