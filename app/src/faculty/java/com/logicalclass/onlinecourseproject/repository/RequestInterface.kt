package com.logicalclass.onlinecourseproject.repository

import com.logicalclass.onlinecourseproject.authentication.model.UserDetails
import com.logicalclass.onlinecourseproject.models.SearchOrgResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RequestInterface {

    @GET("checkOrgFaculty")
    fun getCheckOrganisation(
        @Query("orgName") orgName: String?
    ): Observable<SearchOrgResponse>

    @GET("facultyLogin")
    fun login(
        @Query("email") email: String,
        @Query("password") password: String,
        @Query("auth") auth: String?,
        @Query("device_token") device_token: String?
    ): Observable<UserDetails>

    @GET("facultyLogin")
    fun forgotPassword(
        @Query("uniqueId") email: String
    ): Observable<UserDetails>
}